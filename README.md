# Desafío Backend

Servicio API REST para obtener el nombre completo del usuario. Para ello, el servicio debe permitir ingresar el usuario y contraseña.

## Tabla de Contenido 📋

- [Desafío Backend](#desafío-backend)
  - [Tabla de Contenido 📋](#tabla-de-contenido-)
  - [Dependencias](#dependencias)
  - [Pre-requisitos](#pre-requisitos)
  - [Variables de entorno](#variables-de-entorno)
      - [Service Config](#service-config)
      - [Config Database](#config-database)
  - [Quick Start](#quick-start)
    - [Docker](#docker)
    - [Local](#local)
  - [API](#api)

## Dependencias
A continuación, se presentarán las dependencias más importantes que se usaron en la construcción del servicio

| Módulo | Versión | Tamaño
| ------ | ------ | ------ |
| mysql2  | 2.3.3 | [![pg](https://packagephobia.com/badge?p=mysql2)](https://github.com/sidorares/node-mysql2) |
| express  | 4.17.2 | [![express](https://packagephobia.com/badge?p=express)](https://github.com/expressjs/express) |
| module-alias  | 2.2.2 | [![install size](https://packagephobia.com/badge?p=module-alias)](https://github.com/ilearnio/module-alias) |
| winston  | 3.3.3 | [![winston](https://packagephobia.com/badge?p=winston)](https://github.com/winstonjs/winston) |
| winston-daily-rotate-file  | 4.5.5 | [![winston-daily-rotate-file](https://packagephobia.com/badge?p=winston-daily-rotate-file@4.5.5)](https://packagephobia.com/winston-daily-rotate-file@4.5.5) |
| sequelize  | 6.21.2 | [![sequelize](https://packagephobia.com/badge?p=sequelize)](https://sequelize.org/docs/v6/getting-started/) |

## Pre-requisitos
Para poder ejecutar el servicio es necesario cumplir con los siguientes pre-requisitos:

1. Tener instalado la versión de node v16.15.1
4. Tener instalado Docker, [clic aquí](https://www.docker.com/get-docker). Solo aplica si se levanta el servicio usando docker.
5. Tener instalado eslint de manera global. Para más información, [clic aquí.](https://eslint.org/docs/user-guide/getting-started). Solo aplica si se levanta de manera local.
6. Tener instalador nodemon de manera global. Para más información, [clic aquí.](https://www.npmjs.com/package/nodemon). Solo aplica si se levanta de manera local.
7. Clonar el servicio del siguiente repositorio, [clic aquí.](https://gitlab.com/servicios-publicos/colera-app)
8. Configurar correctamente las variables de entorno. Ver sección **Variables de entorno**


## Variables de entorno
Crear el archivo `.env` en la raíz del proyecto y agregar las siguientes variables de entorno. En el repositorio encontraras un archivo `.env-sample` con valores de ejemplos.

#### Service Config
```
NODE_ENV                = Entorno de desarrollo
NODE_PORT               = Puerto en el que el servicio será levantado
NODE_PREFIX             = Prefijo usado para las rutas, por ejemplo: /api
```
#### Config Database
```
DB_DIALECT                          = Valor que indica a qué tipo de base de datos se conectará el servicio. Para el test se ha usado 'mysql'. Más información en el siguiente link https://sequelize.org/docs/v6/getting-started/
DB_HOST                             = Host en donde se encuentre la base de datos
DB_PORT                             = Puerto de ejecución de la base de datos
DB_USER                             = Usuario de la base de datos
DB_PASSWORD                         = Contraseña del usuario
DB_DATABASE                         = Nombre de la base de datos
```

## Quick Start

Para levantar el servicio tenemos 2 opciones: Local o docker. A continuación, se explicará los pasos que se debe tener en cuenta para levantar el servicio con uno de los 2 métodos mencionados líneas arriba.

### Docker

* Tener instalado docker
* Ubicarse en la carpeta raíz del servicio
* Construir la imagen del servicio ejecuntado el siguiente comando
```bash
docker build . -t solera-service
```
* Levantar el contenedor usando la imagen creada previamente ejecutando el comando
```bash
docker run --name solera-service -p 4070:4070 -d solera-service
```

### Local
* Ubicarse en la carpeta raíz del servicio
* Instalar los módulos usando el comando
```bash
$ npm intall || yarn run install
```
* Levantar el servicio ejecutando el comando
```bash
$ npm run dev || yarn run dev
```

## API
La documentación de la API podrás encotrar en la siguiente ruta, [clic aquí](https://documenter.getpostman.com/view/5107766/UzJFxKHz).