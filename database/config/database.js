const dotenv = require(`dotenv`)

const envFound = dotenv.config()
if (envFound.error) {
  // This error should crash whole process
  throw new Error(`⚠️  Couldn't find .env file - Migration config ⚠️`)
}

module.exports = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT
  }
}
