const { Sequelize } = require(`sequelize`)

// Loader database
const connection = require(`@loaders/database`)

// Model dedinition
const User = connection.define(`User`, {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: Sequelize.STRING,
    validate: { notEmpty: true }
  },
  password: {
    type: Sequelize.STRING,
    validate: { notEmpty: true }
  },
  fullname: {
    type: Sequelize.STRING,
    validate: { notEmpty: true }
  },
  createdAt: { type: Sequelize.DATE },
  updatedAt: { type: Sequelize.DATE }
}, { schema: process.env.DB_SCHEMA })

module.exports = User
