// Import modules
const { Sequelize } = require(`sequelize`)

// Environment variables
const config = require(`@/config`)
const { services } = config
const { database } = services
const { host, dialect, database: databaseName, user, password } = database

// Connecting to a database
const connection = new Sequelize(databaseName, user, password, { host, dialect })

module.exports = connection
