exports.generateResponse = options => {
  const { success, data, error } = options

  return { success, data, error }
}

exports.templateSuccess = data => this.generateResponse({ success: true, data, error: null })

exports.templateError = error => this.generateResponse({ success: false, data: null, error })
