const express = require(`express`)
const route = express.Router()

// UsersController
const { singin } = require(`@api/controllers/Http/UsersController`)

module.exports = ({ app }) => {
  app.use(`/users`, route)

  route.post(`/singin`, singin())
}
