const express = require(`express`)
const { Router } = express

// Routes
const users = require(`./routes/users`)

// Inject routes
module.exports = () => {
  const app = Router()

  // Routes for users api
  users({ app })

  return app
}
