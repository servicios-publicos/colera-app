"user strict"

const bcrypt = require(`bcrypt`)

// Models
const User = require(`@database/models/user`)

// Helpers
const ResponseHelper = require(`@helpers/Response`)

// Init logger
const path = require(`path`)
const scriptName = path.basename(__filename)
const logger = require(`@loaders/logger`)(scriptName)

exports.singin = function() {
  return async (request, response) => {
    try {
      // Object to store the result
      let formatResult = {}

      // Get params
      const { body } = request
      const { username, password } = body

      if (!(username && password)) {
        formatResult = { result: `Username and password is required`, statusCode: 401 }
      } else {
        // Find the user
        const user = await User.findOne({ where: { username } })
        if (user) {
          // Check user password with hashed password stored in the database
          const validPassword = await bcrypt.compare(password, user.password)
          if (validPassword) {
            formatResult = { result: `¡${user.fullname}, welcome to the system!`, statusCode: 200 }
          } else {
            formatResult = { result: `Oops! ${user.fullname} the password entered is incorrect`, statusCode: 401 }
          }
        } else {
          formatResult = { result: `Oops! Wrong username and/or password`, statusCode: 400 }
        }
      }

      // Return result
      const formatSuccess = ResponseHelper.templateSuccess(formatResult.result)
      response.status(formatResult.statusCode).send(formatSuccess)
    } catch (error) {
      logger.error(error)
      const formatError = ResponseHelper.templateError(error.message)
      response.status(500).send(formatError)
    }
  }
}
