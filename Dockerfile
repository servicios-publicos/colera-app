# Crear una nueva capa apartir de Node v14
FROM node:16.15.1

LABEL maintainer="Solera App"
LABEL description="Servicio que obtener la información del usuario"
LABEL version="v1.0.0"

# Install dependency nodemon
RUN yarn global add nodemon

# Specify root directory
ENV HOME=/usr/src/app
WORKDIR $HOME

# Install package modules
COPY package.json ./
RUN yarn install
COPY . .

# Show all files
RUN ls -lha

# Up service
CMD [ "yarn", "run", "dev" ]
